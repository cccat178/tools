%%%-------------------------------------------------------------------
%%% @author shisj
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%% 将文件的二进制信息转换为string型 md5码
%%% @end
%%% Created : 07. 七月 2016 11:07
%%%-------------------------------------------------------------------
-module(md5).
-author("shisj").

%% API
-export([md5_to_string/1]).

md5_to_string(Binary) ->
  Md5_bin =  erlang:md5(Binary),
  Md5_list = binary_to_list(Md5_bin),
  lists:flatten(list_to_hex(Md5_list)).

list_to_hex(L) ->
  lists:map(fun(X) -> int_to_hex(X) end, L).

int_to_hex(N) when N < 256 ->
  [hex(N div 16), hex(N rem 16)].

hex(N) when N < 10 ->
  $0+N;
hex(N) when N >= 10, N < 16 ->
  $a + (N-10).
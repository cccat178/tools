@echo off

rem 设置目录变量
set root=%CD%
set BASE_DIR=%root%\ZTools

rem 设置启动文件
set Startup=%BASE_DIR%\tools.beam
rem 设置erl运行的模块方法
set START_MOD_FUNC=tools start

rem 运行
erl -pa %BASE_DIR% -noshell -s %START_MOD_FUNC%